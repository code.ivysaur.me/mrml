# mrml

![](https://img.shields.io/badge/written%20in-PHP%2C%20Javascript-blue)

A forth-like formula expression evaluator.

This was eventually ported to Javascript to allow identical front- and back-end implementations.

Tags: PL


## Download

- [⬇️ mrml-1.zip](dist-archive/mrml-1.zip) *(1.77 KiB)*
